import axios from "axios";

/**
 * Under the hood doing axios request to the API endpoint
 *
 * @param {string} endpoint the API endpoint
 * @param {boolean} withToken need token or not / API endpoint protected or not
 * @param {object} options options for request with two properties: method and body, i.e. { method: 'POST', body: { title: 'Task 1', description: 'Lorem ipsum' } }
 * @param {object} head headers for request, e.g. { 'Content-Type' : 'application/json'  }
 * @returns
 */
export async function fetchFrom(endpoint, withToken, options, head) {
    try {
        const headers = withToken
            ? { token: localStorage.getItem("token"), ...head }
            : { ...head };
        const method = options ? options.method : "GET";
        const data = options ? options.body : "";

        const response = await axios({
            url: `${process.env.REACT_APP_API_BASE_URL}/api${endpoint}`,
            method,
            headers,
            data,
        });

        return {
            ...response.data,
            status: response.status,
            statusText: response.statusText,
        };
    } catch (error) {
        if (error.response) {
            throw error.response;
        }

        throw error;
    }
}

/**
 * Create a new todo task
 *
 * @param {object} body data that will be submitted
 * @returns
 */
export const postTodo = async (body) => {
    return fetchFrom(
        "/todos/add",
        false,
        { method: "POST", body },
        { "Content-Type": "application/json" }
    );
};

/**
 * Get all todo tasks
 *
 * @returns
 */
export const getTodos = async () => {
    return fetchFrom("/todos", false);
};

/**
 * Mark a task as complete
 *
 * @param {number | string} todoId task id
 * @returns
 */
export const completeTodo = async (todoId) => {
    return fetchFrom(`/todos/complete/${todoId}`, false, { method: "PUT" });
};

/**
 * Delete a task
 *
 * @param {number | string} todoId
 * @returns
 */
export const deleteTodo = async (todoId) => {
    return fetchFrom(`/todos/${todoId}`, false, { method: "DELETE" });
};
