import { useState, useEffect } from "react";
import InputForm from "./components/InputForm";
import TodoList from "./components/TodoList";
import { postTodo, completeTodo, getTodos, deleteTodo } from "./api";

function App() {
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        async function fetchTodos() {
            try {
                const response = await getTodos();
                setTodos(response.results);
            } catch (error) {
                console.log({ error });
            }
        }

        fetchTodos();
    }, []);

    const handleAddTodo = async (data) => {
        try {
            const newTodo = { ...data };
            const response = await postTodo(newTodo);
            setTodos((prev) => [...prev, response.result]);
        } catch (error) {
            console.log({ error });
        }
    };

    const handleCheckTodo = async (todoId) => {
        try {
            const response = await completeTodo(todoId);
            console.log({ todoId });
            console.log({ response });
            setTodos(response.results);
        } catch (error) {
            console.log({ error });
        }
    };

    const handleDeleteTodo = async (todoId) => {
        try {
            const response = await deleteTodo(todoId);
            console.log({ todoId });
            console.log({ response });
            setTodos(response.results);
        } catch (error) {
            console.log({ error });
        }
    };

    return (
        <div className="container py-5">
            <div className="card">
                <div className="card-body">
                    <div className="row" style={{ height: "85vh" }}>
                        <div className="col-4" style={{ height: "100%" }}>
                            <h1>Todo App</h1>
                            <hr />
                            {/* Input Form */}
                            <InputForm onSubmit={handleAddTodo} />
                        </div>

                        {/* Todo List */}
                        <div className="col-8" style={{ height: "100%" }}>
                            <TodoList
                                todos={todos}
                                onComplete={handleCheckTodo}
                                onDelete={handleDeleteTodo}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
