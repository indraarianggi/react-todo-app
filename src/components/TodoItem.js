import React from "react";

const TodoItem = ({ todo, onComplete, onDelete }) => {
    const { title, description, complete } = todo;

    const checkedTodo = todo.complete ? `line-through` : "";

    return (
        <div className="card mb-4">
            <div className="card-body">
                <div className="row">
                    <div className="col-10">
                        <h4 className={checkedTodo}>{title}</h4>
                        <p className={checkedTodo}>{description}</p>
                    </div>
                    <div className="col-2">
                        {!complete && (
                            <button
                                className="btn btn-sm btn-block btn-primary mb-2"
                                onClick={() => onComplete(todo.id)}
                            >
                                complete
                            </button>
                        )}
                        <button
                            className="btn btn-sm btn-block btn-danger"
                            onClick={() => onDelete(todo.id)}
                        >
                            delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TodoItem;
