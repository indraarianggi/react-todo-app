import React, { useState } from "react";

const InputForm = ({ onSubmit }) => {
    const [form, setForm] = useState({ title: "", description: "" });

    const handleChange = (e) => {
        setForm({ ...form, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(form);
        setForm({ title: "", description: "" });
    };

    return (
        <form>
            <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                    className="form-control"
                    type="text"
                    name="title"
                    id="title"
                    value={form.title}
                    onChange={(e) => handleChange(e)}
                />
            </div>
            <div className="form-group">
                <label htmlFor="description">Description</label>
                <textarea
                    className="form-control"
                    name="description"
                    id="description"
                    rows="10"
                    value={form.description}
                    onChange={(e) => handleChange(e)}
                />
            </div>
            <button
                className="btn btn-success btn-block"
                onClick={(e) => handleSubmit(e)}
            >
                Add
            </button>
        </form>
    );
};

export default InputForm;
