import React from "react";
import TodoItem from "./TodoItem";

const TodoList = ({ todos, onComplete, onDelete }) => {
    return (
        <div style={{ height: "100%", overflowX: "hidden" }}>
            <div className="row">
                <div className="col">
                    <h2 className="mb-4">Todo List</h2>
                </div>
            </div>
            <div className="row" style={{ height: "100%", overflowX: "auto" }}>
                <div className="col">
                    {/* render all todo items */}
                    {todos.length > 0 ? (
                        todos.map((item) => (
                            <TodoItem
                                key={item.id}
                                todo={item}
                                onComplete={onComplete}
                                onDelete={onDelete}
                            />
                        ))
                    ) : (
                        <p className="text-center">No datas</p>
                    )}
                </div>
            </div>
        </div>
    );
};

export default TodoList;
